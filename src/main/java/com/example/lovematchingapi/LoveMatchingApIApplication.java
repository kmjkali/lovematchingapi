package com.example.lovematchingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoveMatchingApIApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoveMatchingApIApplication.class, args);
    }

}
