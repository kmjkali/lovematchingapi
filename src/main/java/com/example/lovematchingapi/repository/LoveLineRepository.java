package com.example.lovematchingapi.repository;

import com.example.lovematchingapi.entity.LoveLine;

import org.springframework.data.jpa.repository.JpaRepository;
public interface LoveLineRepository extends JpaRepository<LoveLine,Long> {
}
