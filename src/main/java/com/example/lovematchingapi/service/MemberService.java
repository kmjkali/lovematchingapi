package com.example.lovematchingapi.service;

import com.example.lovematchingapi.entity.Member;
import com.example.lovematchingapi.model.member.MemberCreateRequest;
import com.example.lovematchingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {

    private final MemberRepository memberRepository;

    public Member getData(long id) {

        return memberRepository.findById(id).orElseThrow();

    }


    public void setMember(MemberCreateRequest request){

        Member addDate = new Member();

        addDate.setName(request.getName());
        addDate.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(addDate);

    }
}
