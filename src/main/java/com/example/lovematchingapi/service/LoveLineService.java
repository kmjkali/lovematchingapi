package com.example.lovematchingapi.service;

import com.example.lovematchingapi.entity.LoveLine;
import com.example.lovematchingapi.entity.Member;
import com.example.lovematchingapi.model.loveline.LoveLineCreateRequest;
import com.example.lovematchingapi.model.loveline.LoveLineItem;
import com.example.lovematchingapi.repository.LoveLineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class LoveLineService {

    private final LoveLineRepository loveLineRepository;



    public void setLoveLine(Member member, LoveLineCreateRequest request) {

        LoveLine addData = new LoveLine();

        addData.setLovePhoneNumber(request.getLovePhoneNumber());

        addData.setMember(member);


        loveLineRepository.save(addData);


    }

    public List<LoveLineItem> getLoveLines(){
        List<LoveLine> originList= loveLineRepository.findAll();
        List<LoveLineItem> result = new LinkedList<>();

        for (LoveLine loveLine:originList) {

            LoveLineItem addItem = new LoveLineItem();
            addItem.setMemberid(loveLine.getMember().getId());
            addItem.setMemberName(loveLine.getMember().getName());
            addItem.setMemberPhoneNumber(loveLine.getMember().getPhoneNumber());
            addItem.setLovePhoneNumber(loveLine.getLovePhoneNumber());


            result.add(addItem);



        }
        return result;
    }



}
