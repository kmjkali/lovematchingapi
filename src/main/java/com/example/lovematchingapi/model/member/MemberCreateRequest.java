package com.example.lovematchingapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class MemberCreateRequest {

    private String Name;
    private String phoneNumber;
}
