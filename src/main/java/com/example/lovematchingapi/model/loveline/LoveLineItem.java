package com.example.lovematchingapi.model.loveline;


import com.example.lovematchingapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoveLineItem {

    private Long memberid;
    private String memberName;
    private String memberPhoneNumber;
    private String lovePhoneNumber;


}
