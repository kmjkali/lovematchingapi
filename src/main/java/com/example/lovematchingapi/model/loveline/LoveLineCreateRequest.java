package com.example.lovematchingapi.model.loveline;

import com.example.lovematchingapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoveLineCreateRequest {


    private String lovePhoneNumber;
}
