package com.example.lovematchingapi.controller;


import com.example.lovematchingapi.entity.Member;
import com.example.lovematchingapi.model.loveline.LoveLineCreateRequest;
import com.example.lovematchingapi.model.loveline.LoveLineItem;
import com.example.lovematchingapi.service.LoveLineService;
import com.example.lovematchingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/love-line")

public class LoveLineController {

    private final MemberService memberService;
    private final LoveLineService loveLineService;

    @PostMapping("/new/member-id/{memberId}")

    public String setLoveLine(@PathVariable long memberId, @RequestBody LoveLineCreateRequest request){

       Member member= memberService.getData(memberId);

        loveLineService.setLoveLine(member,request);

        return "ooh";

    }

    @GetMapping("/all")

    public List<LoveLineItem> getLoveline(){
        return loveLineService.getLoveLines();


    }




}
